package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public int index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialog d = new dialog(MainActivity.this);
        d.show();
        d.b.setText("发送");
        ArrayList<String> listItems= new ArrayList<String>();
        ArrayAdapter<String> adapter=new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,listItems);
        d.b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItems.add(d.e.getText().toString());
                d.LV_right.setAdapter(adapter);
                index++;

            }
        });
//        View view = getLayoutInflater().inflate(R.layout.layout,null);
//        AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
//        b.setTitle("Button");
//        b.setView(view);
//        final EditText t = (EditText) view.findViewById(R.id.editTextText);
//        b.setPositiveButton("Button", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(MainActivity.this, t.getText().toString(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        b.show();
    }
}
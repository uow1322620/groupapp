package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;

public class dialog extends Dialog {
    public EditText e;
    public Button b;
    public ListView LV_right;
    public dialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        e = (EditText) findViewById(R.id.editTextText);
        b = (Button) findViewById(R.id.button);
        LV_right = (ListView) findViewById(R.id.listviewright);
    }
}
